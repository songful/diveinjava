package socket;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class HTTPClient {

	private String host;
	private int port;
	private Socket socket;

	public void createSocket(String host, int port) throws UnknownHostException, IOException{
		socket = new Socket(host, port);
	}

	public void communicate() throws IOException{
		StringBuffer sb=new StringBuffer("GET "+ "/xkzw123/86023.html" +" HTTP/1.1\r\n");
	    sb.append("Host: www.kanshuwo.com\r\n");
	    sb.append("Accept: */*\r\n");
	    sb.append("Accept-Language: zh-cn\r\n");
	    sb.append("Accept-Encoding: gzip, deflate\r\n");
	    sb.append("User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)\r\n");
	    sb.append("Connection: Keep-Alive\r\n\r\n");

	    OutputStream socketOut = socket.getOutputStream();
	    socketOut.write(sb.toString().getBytes());
	    socket.shutdownOutput();
	    InputStream socketIn = socket.getInputStream();
	    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	    byte[] buff = new byte[1024];
	    int len = -1;
	    while ((len = socketIn.read(buff)) != -1) {
			buffer.write(buff);
		}
	    System.out.println(new String(buffer.toByteArray())); //把字节数组转换为字符串
	    socket.close();
	}

	public static void main(String[] args){
		HTTPClient client = new HTTPClient();

		try {
			client.createSocket("www.kanshuwo.com", 80);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			System.out.println("unknown host");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			client.communicate();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
