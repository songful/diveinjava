package socket;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class EchoService {
	private int port = 12345;
	private ServerSocket service;

	public EchoService() throws IOException{
		service = new ServerSocket(port);
		System.out.println("Service is started...");
	}

	public static String echo(String message){
		return "Echo: " + message;
	}

	private static PrintWriter getWriter(Socket socket) throws IOException{
		OutputStream sockeOut = socket.getOutputStream();
		return new PrintWriter(sockeOut, true);
	}

	private static BufferedReader getReader(Socket socket) throws IOException{
		InputStream socketIn = socket.getInputStream();
		return new BufferedReader(new InputStreamReader(socketIn));
	}

	public void service() throws IOException {
		while(true){
			Socket socket = service.accept();
			invoke(socket);
		}
	}

	static void invoke(final Socket socket){
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					System.out.println("New Connection accepted: "  + socket.getInetAddress() + ": " + socket.getPort());

					BufferedReader reader = getReader(socket);
					PrintWriter writer = getWriter(socket);

					String message = null;
					while((message = reader.readLine()) != null){
						System.out.println(message);
						writer.println(echo(message));

						if(message.equals("bye")){
							writer.println(echo("Bye"));
							break;
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				}finally{
					if(socket != null){
						try {
							socket.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}

		}).start();
	}

	public static void main(String[] args){
		try {
			new EchoService().service();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
