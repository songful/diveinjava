package socket;

import java.io.*;
import java.net.Socket;

public class EchoClient {
	private String host;
	private int port;
	private Socket client;

	public EchoClient(String host,int port) throws IOException {
			client = new Socket(host, port);

	}

	private PrintWriter getWriter(Socket socket) throws IOException {
		OutputStream sockeOut = socket.getOutputStream();
		return new PrintWriter(sockeOut, true);
	}

	private BufferedReader getReader(Socket socket) throws IOException {
		InputStream socketIn = socket.getInputStream();
		return new BufferedReader(new InputStreamReader(socketIn));
	}

	public void talk() {
		try {
			BufferedReader reader = getReader(client);
			PrintWriter writer = getWriter(client);

			BufferedReader localReader = new BufferedReader(
					new InputStreamReader(System.in));
			String message = null;
			while ((message = localReader.readLine()) != null) {
				writer.println(message);
				System.out.println(reader.readLine());
				if (message.equals("bye")) {
					break;
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (client != null) {
				try {
					client.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public static void main(String[] args){
		try {
			new EchoClient("127.0.0.1", 12345).talk();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
