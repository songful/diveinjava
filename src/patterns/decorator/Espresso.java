package patterns.decorator;
/**
 * 具体被装饰者类
 * @author Songful
 *
 */
public class Espresso extends Beverage {

	public Espresso(){
		description = "Espresso";
	}

	@Override
	public double cost() {
		// TODO Auto-generated method stub
		return 1.99;
	}

}
