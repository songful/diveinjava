package patterns.decorator;
/**
 * 饮料, 抽象被装饰者
 * @author Songful
 *
 */
public abstract class Beverage {
	String description = "unknown Beverage";

	public String getDescription(){
		return description;
	}

	public abstract double cost();
}
