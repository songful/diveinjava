package patterns.decorator;
/**
 * 抽象装饰者类
 * @author Songful
 *
 */
public abstract class CondimentDecorator extends Beverage {
	public abstract String getDescription();
}
