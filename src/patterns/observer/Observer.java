package patterns.observer;
/**
 * 观察者接口，所有潜在的观察者必须实现此接口，
 * 只有update方法，主题改变时调用此方法
 * @author Songful
 *
 */
public interface Observer {
	public void update(String temperature, String humidity, String pressure);
}
