package patterns.observer;
/**
 * 发布显示接口，发布者实现此接口的display() 方法
 * @author Songful
 *
 */
public interface DisplayElement {
	public void display();
}
