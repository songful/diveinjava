package patterns.observer;

public class CurrentConditionsDisplay implements Observer, DisplayElement {

	private String temperature;
	private String humidity;
	private String pressure;

	private Subject weatherData;

	public CurrentConditionsDisplay(Subject weatherData) {
		this.weatherData = weatherData;
		weatherData.registerObserver(this);
	}

	@Override
	public void display() {
		System.out.println("Current Conditions are: " + temperature + " "
				+ humidity + " " + pressure);
	}

	@Override
	public void update(String temperature, String humidity, String pressure) {
		this.setTemperature(temperature);
		this.setHumidity(humidity);
		this.setPressure(pressure);
		display();

	}

	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}

	public void setHumidity(String humidity) {
		this.humidity = humidity;
	}

	public void setPressure(String pressure) {
		this.pressure = pressure;
	}


}
