package patterns.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * 主题对象，此处为天气信息
 * @author Songful
 *
 */
public class WeatherData implements Subject {

	private String temperature;
	private String humidity;
	private String pressure;

	private List<Observer> observers;

	public WeatherData(){
		observers = new ArrayList<Observer>();
	}

	@Override
	public void registerObserver(Observer observer) {
		if(!observers.contains(observer)){
			observers.add(observer);
		}else{
			System.out.println("This observer has been registered!");
		}

	}

	@Override
	public void removeObserver(Observer observer) {
		if(observers.contains(observer)){
			observers.remove(observer);
		}else{
			System.out.println("This observer does't in register!");
		}
	}

	@Override
	public void nodifyObservers() {
		for(Observer ob:observers){
			ob.update(temperature, humidity, pressure);
		}
	}

	public void dataChanged(){
		nodifyObservers();
	}

	public void setData( String temperature, String humidity, String pressure){
		this.temperature = temperature;
		this.humidity = humidity;
		this.pressure = pressure;
		dataChanged();
	}

	public String getTemperature() {
		return temperature;
	}

	public String getHumidity() {
		return humidity;
	}

	public String getPressure() {
		return pressure;
	}
}
