package patterns.observer;
/**
 *主题接口，对象使用此接口注册为观察者，或把自己从观察者中删除
 * @author Songful
 *
 */
public interface Subject {
	public void registerObserver(Observer observer);
	public void removeObserver(Observer observer);
	public void nodifyObservers();
}
