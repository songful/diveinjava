package concurrency;

public class TestThread {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Thread test1 = new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try{
					int i =0;
					while(i++ <10000){
						//...
					}
					System.out.println("A1");
				}catch(Exception e){
					System.out.println("B1");
					System.out.println(e);
				}
			}
		});

		test1.start();test1.interrupt();


		Thread test2 = new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try{
					Thread.sleep(10000);
					System.out.println("A2");
				}catch(Exception e){
					System.out.println("B2");
					System.out.println(e);
				}
			}
		});

		test2.start();test2.interrupt();

		Thread test3 = new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try{
					//Thread.sleep(10000);
					this.wait(10000);
					System.out.println("A3");
				}catch(Exception e){
					System.out.println("B3");
					System.out.println(e);
				}
			}
		});

		test3.start();test3.interrupt();

		Thread test4 = new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try{
					synchronized (this) {
						this.wait(10000);
					}
					System.out.println("A4");
				}catch(Exception e){
					System.out.println("B4");
					System.out.println(e);
				}
			}
		});

		test4.start();test4.interrupt();

		try{
			test4.start();
			System.out.println("A5");
		}catch(Exception e){
			System.out.println("B5");
			System.out.println(e);
		}
	}

}
