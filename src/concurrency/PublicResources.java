package concurrency;

/**
 * 公共资源类
 */
public class PublicResources {
	private int number = 0;

	/**
	 * 增加公共资源
	 */
	public synchronized void increace() {
		while (number != 0) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		number++;
		System.out.println(" 生产 1 现在： "+number);
		notify();
	}

	/**
	 * 减少公共资源
	 */
	public synchronized void decreace() {
		while (number == 0) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		number--;
		System.out.println(" 消费 1 现在： "+number);
		notify();
	}
}
