﻿package concurrency;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * User: Songful
 * Date: 13-3-6
 * Time: 下午9:46
 * To change this template use File | Settings | File Templates.
 */
public class NotifyVsNotifyAll {
    public static void main(String[] args) throws Exception{
        ExecutorService exec = Executors.newCachedThreadPool();
        for(int i = 0; i < 5; i++ ){
            exec.execute(new Task1());}

        exec.execute(new Task2());

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            boolean prod = true;
            @Override
            public void run() {
                if(prod){
                    System.out.println("notify...");
                    Task1.blocker.prod();
                    prod = false;
                }else{
                    System.out.println("notifyAll...");
                    Task1.blocker.prodAll();
                    prod = true;
                }
            }
        },400,400);

        TimeUnit.SECONDS.sleep(5);
        timer.cancel();
        System.out.println("\nTimer canceled");
        TimeUnit.MILLISECONDS.sleep(500);
        System.out.println("Task2.blocker.prodAll() ");
        Task2.blocker.prodAll();
        TimeUnit.MILLISECONDS.sleep(500);
        System.out.println("\nShutting down");
        exec.shutdownNow(); // Interrupt all tasks

    }
}

class Blocker{
    synchronized void waitingCall(){
        try{
            while(!Thread.interrupted()){
                wait();
                System.out.println("The current Thread is " + Thread.currentThread());
            }
        }catch (InterruptedException e){
            ;
        }
    }

    synchronized void prod(){notify();}
    synchronized void prodAll(){notifyAll();}
}

class Task1 implements Runnable{
    static Blocker blocker = new Blocker();
    @Override
    public void run(){
        blocker.waitingCall();
    }
}

class Task2 implements Runnable{
    static Blocker blocker = new Blocker();
    @Override
    public void run(){
        blocker.waitingCall();
    }
}