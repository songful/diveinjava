package concurrency;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA. User: Songful Date: 13-3-6 Time: 下午11:03 To
 * change this template use File | Settings | File Templates.
 */
public class ProducerConsumer {
	public static void main(String[] args) {
		PublicResource resource = new PublicResource();
	/*	ExecutorService exec = Executors.newCachedThreadPool();
		for (int i = 0; i < 3; i++) {
			exec.execute(new Producter(resource));
		}
		for (int j = 0; j < 3; j++) {
			exec.execute(new Consumer(resource));
		}

		try {
			TimeUnit.MILLISECONDS.sleep(999);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		exec.shutdownNow();*/
//		System.out.println("exiting...");
		/*
		 * new Thread(new Producter(resource)).start(); new Thread(new
		 * Consumer(resource)).start(); new Thread(new
		 * Producter(resource)).start(); new Thread(new
		 * Consumer(resource)).start(); new Thread(new
		 * Producter(resource)).start(); new Thread(new
		 * Consumer(resource)).start();
		 */
		new Thread(new Producter(resource)).start();
		new Thread(new Consumer(resource)).start();
	}
}

class PublicResource {
	int count = 0;

	public synchronized void increase() { //
		while (count == 10) {
			try {
				this.wait();
			} catch (InterruptedException e) {
//				System.out.println(" not enough, give me more....");
			}
		}
		// int j = (int) (Math.random()*10);
//		int j = 1;
		count++;
		System.out.println("Resource increased " + 1 + ", and resource is "
				+ count);
		this.notify();
	}

	public synchronized void reduce() { //
	// int i = (int) (Math.random()*10);
		int i = 1;
		if (count == 0) {
			try {
				wait();
			} catch (InterruptedException e) {
//				System.out.println("resource is enough, enjoy...");
			}
		}
		count--;
		System.out.println("Resource reduced " + 1 + ", and resource is "
				+ count);
		this.notify();

	}
}

class Producter implements Runnable {
	private PublicResource resource;

	public Producter(PublicResource resource) {
		this.resource = resource;
	}

	public void run() {
		for (int i = 0; i < 10; i++) {
			resource.increase();
			try {
				// Thread.sleep((long) (Math.random()*1000));
				Thread.sleep(100);
			} catch (InterruptedException e) {
//				System.out.println("give me more... not enough");
			}

		}
	}
}

class Consumer implements Runnable {
	private PublicResource resource;

	public Consumer(PublicResource resource) {
		this.resource = resource;
	}

	public void run() {
		for (int i = 0; i < 20; i++) {
			resource.reduce();
			try {
				// Thread.sleep((long) (Math.random()*1000));
				Thread.sleep(200);
			} catch (InterruptedException e) {
//				System.out.println("enjoy yourself...");
			}

		}
	}
}