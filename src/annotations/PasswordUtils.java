package annotations;

import java.util.List;

public class PasswordUtils{

	@UseCase(id=12, description="password must contain at least one numeric")
	public boolean validatePassword(String password){
		return password.matches("\\w*\\d\\w*");
	}

	@UseCase(id = 13)
	public String encryptPassword(String password){
		return new StringBuilder(password).reverse().toString();
	}

	@UseCase(id = 14, description="new password can't be equal to previously used ones")
	public boolean checkForNewPassword(
		List<String> prevPasswords, String newPassword){
		return !prevPasswords.contains(newPassword);
	}
}