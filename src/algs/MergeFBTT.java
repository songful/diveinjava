package algs;

/**
 * buttom-up merge sort(Non-recursive)
 * User: Songful
 * Date: 13-8-28 下午8:27
 * Description: MergeFBTT
 */
public class MergeFBTT {
	/**
	 *
	 * @param a
	 * @param aux
	 * @param low
	 * @param mid
	 * @param hi
	 */
	private static void merge(Comparable[] a, Comparable[] aux, int low,
			int mid, int hi) {
		// copy to aux
		for (int i = low; i <= hi; i++) {
			aux[i] = a[i];
		}

		int i = low, j = mid + 1;
		for (int k = low; k <= hi; k++) {
			if (i > mid)
				a[k] = aux[j++];
			else if (j > hi)
				a[k] = aux[i++];
			else if (less(aux[j], aux[i]))
				a[k] = aux[j++];
			else
				a[k] = aux[i++];
		}
	}

	/**
	 * buttom-up mergesort
	 *
	 * @param a
	 */
	public static void sort(Comparable[] a) {
		int N = a.length;
		Comparable[] aux = new Comparable[N];
		// n=1是，步长为1，merge a[0-1] a[1-2]...
		// 步长为2， merge a[0-3] a[4-7] ...
		// ...
		// 步长为N-1 merge a[0 - n-1]
		for (int n = 1; n < N; n = n + n) {
			for (int i = 0; i < N - n; i += n + n) {
				int low = i;
				int mid = i + n - 1;
				int hi = Math.min(i + n + n - 1, N - 1);
				merge(a, aux, low, mid, hi);
			}
		}
	}

	// is v < w ?
	private static boolean less(Comparable v, Comparable w) {
		return (v.compareTo(w) < 0);
	}

	// exchange a[i] and a[j]
	private static void exch(Object[] a, int i, int j) {
		Object swap = a[i];
		a[i] = a[j];
		a[j] = swap;
	}

	/***********************************************************************
	 * Check if array is sorted - useful for debugging
	 ***********************************************************************/
	private static boolean isSorted(Comparable[] a) {
		for (int i = 1; i < a.length; i++)
			if (less(a[i], a[i - 1]))
				return false;
		return true;
	}

	// print array to standard output
	private static void show(Comparable[] a) {
		for (int i = 0; i < a.length; i++) {
			System.out.println(a[i]);
		}
	}

	public static void main(String[] args){
		Integer[] a = {2,4,3,13,5,7,6,9,12,1,8};
		sort(a);
		show(a);
	}
}
