package algs;

import java.util.Stack;

public class Graph_DFPaths {
	private boolean[] marked;
	private int[] edgeTo;
	private final int s;

	public Graph_DFPaths(Graph graph , int s) {
		this.s = s;
		marked = new boolean[graph.V()];
		edgeTo = new int[graph.V()];
		dfs(graph, s);
	}

	public void dfs(Graph graph, int s){
		marked[s] = true;
		for(int w : graph.adj(s)){
			if(!marked[w]){
				edgeTo[w] = s;
				dfs(graph, w);
			}
		}
	}

	public boolean hasPathTo(int v){
		return marked[v];
	}

	public Iterable<Integer> pathTo(int v){
		if(!hasPathTo(v)) return null;
		Stack<Integer> path = new Stack<Integer>();
		for(int x = v; x != s; x = edgeTo[x]){
			path.push(x);
		}
		path.push(s);
		return path;
	}
}
