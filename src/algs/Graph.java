package algs;

import java.util.ArrayList;
import java.util.List;

/**
 * 无向图
 * @author Songful
 *
 */
public class Graph {
	private final int V;
	private int E;
	private List<Integer>[] adj;

	public Graph(int V){
		this.V = V;
		this.E = 0;
		adj = (List<Integer>[]) new List[V];
		for(int v=0;v<V;v++){
			adj[v] = new ArrayList<Integer>();
		}
	}

	public int V(){
		return V;
	}

	public int E(){
		return E;
	}

	public void addAdj(int v, int w){
		adj[v].add(w);
		adj[w].add(v);
		E++;
	}

	public Iterable<Integer> adj(int v){
		return adj(v);
	}
}
