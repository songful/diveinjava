package algs;

import java.util.Queue;
import java.util.LinkedList;
import java.util.Stack;

public class Graph_BFPath {
	private boolean[] marked;
	private int[] edgeTo;
	private final int s;

	public Graph_BFPath(Graph graph , int s) {
		this.s = s;
		marked = new boolean[graph.V()];
		edgeTo = new int[graph.V()];
		bfs(graph, s);
	}

	public void bfs(Graph graph, int s){
		Queue<Integer> queue = new LinkedList<Integer>();
		marked[s] = true;
		queue.offer(s);
		while(!queue.isEmpty()){
			int v = queue.poll();
			for(int w : graph.adj(v)){
				if(!marked[w]){
					marked[w] = true;
					edgeTo[w] = v;
					queue.offer(w);
				}
			}
		}
	}

	public boolean hasPathTo(int v){
		return marked[v];
	}

	public Iterable<Integer> pathTo(int v){
		if(!hasPathTo(v)) return null;
		Stack<Integer> path = new Stack<Integer>();
		for(int x = v; x != s; x = edgeTo[x]){
			path.push(x);
		}
		path.push(s);
		return path;
	}
}
