package algs;

public class HeapSort {

	/*
	 * 堆排序其实是利用了用数组表示完全二叉树的一个特点，那就是完全二叉树上某个节点的编号k,k*2和k*2+1是k节点 的左右节点的编号
	 * 堆排序有两个步骤：1。如何将一个无序序列建成一个堆序列 2。如何在输出堆顶元素之后调整剩余元素成为一个新的堆。
	 */

	private static int[] getHeap(int[] a){
		int N = a.length;
		int heap[] = new int[N +1];
		for(int i =1; i<=N; i++){
			heap[i] = a[i-1];
		}

		for(int j = N/2; j>0;j--){
			sink(heap, j , N );
		}

		return heap;
	}

	public static int[] sort(int[] a){
		int N = a.length;
		int[] heap = getHeap(a);
		while(N > 1){
			exch(heap, 1, N--);
			sink(heap, 1, N);
		}
		return heap;
	}

	private static void sink(int[] heap, int i,int N){
		while(2*i <= N){
			int j = 2*i;
			if(j<N && less(heap, j, j+1)) j++;
			if(!less(heap, i, j)) break;
			exch(heap, i, j);
			i = j;
		}
	}

	private static boolean less(int[] a, int i, int j){
		return a[i] < a[j];
	}

	private static void exch(int[] a, int i, int j){
		int tmp = a[i];
		a[i] = a[j];
		a[j] = tmp;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] array = {1,2,3,4,5,6,7,8,9,10,11,12,13};
		int[] heap = sort(array);
		for(int i=1; i<heap.length;i++){
			System.out.println(heap[i]);
		}
	}















	/*private int[] heapData;
	private int numberOfElement;

	public HeapSort(int[] array) {
		heapData = new int[array.length + 1];
		numberOfElement = array.length;
		//堆数组中首位不用
		for (int i = 0; i < array.length; i++) {
			heapData[i + 1] = array[i];
		}

	}

	// 将一个无序序列建立成一个堆序列，
	public void buildHeapArray() {
		int start = numberOfElement / 2;
		for (int j = start; j > 0; j--) {
			int tmp_j = j;
			buileHeap(tmp_j, numberOfElement);
		}
	}

	private void buileHeap(int tmp_j, int num) // 使某个元素的左右子树全部都是堆序列
	{
		// 当结点元素与其子节点交换之后，仍需查看子节点与其子节点之间是否需要进行交换，所以要注意这里需用循环条件来控制
		while (tmp_j <= num / 2) {
			if (tmp_j * 2 + 1 > num) {
				if (heapData[tmp_j] < heapData[2 * tmp_j]) {
					heapData[0] = heapData[tmp_j];
					heapData[tmp_j] = heapData[2 * tmp_j];
					heapData[2 * tmp_j] = heapData[0];
					tmp_j = 2 * tmp_j;
				} else
					break;
			} else {
				if (heapData[tmp_j * 2] > heapData[tmp_j * 2 + 1]) {
					if (heapData[tmp_j] < heapData[2 * tmp_j]) {
						heapData[0] = heapData[tmp_j];
						heapData[tmp_j] = heapData[2 * tmp_j];
						heapData[2 * tmp_j] = heapData[0];
						tmp_j = 2 * tmp_j;
					} else
						break;
				} else {
					if (heapData[tmp_j] < heapData[2 * tmp_j + 1]) {
						heapData[0] = heapData[tmp_j];
						heapData[tmp_j] = heapData[2 * tmp_j + 1];
						heapData[2 * tmp_j + 1] = heapData[0];
						tmp_j = 2 * tmp_j + 1;
					} else
						break;
				}
			}
		}
	}

	public void sort() {
		buildHeapArray();
		for (int j = numberOfElement; j > 0;) {
			heapData[0] = heapData[1];
			heapData[1] = heapData[j];
			heapData[j] = heapData[0];
			j--;
			if (j > 0) {
				buileHeap(1, j);
			}
		}

	}

	public void print() {
		for (int i = 1; i <= numberOfElement; i++) {
			System.out.print(heapData[i] + "     ");
		}
	}
*/


}
