package algs;


/**
 * 简单排序算法，以递增排序为例
 * User: Songful
 * Date: 13-3-25  下午9:34
 */
public class BaseSort {

    private static Comparable[] aux;

    /**
     * 选择排序
     * 查找数组中最小元素，然后与第一个元素交换，除第一个元素外查找最小的元素与
     * 第二个元素交换，然后依次查找并交换元素
     * 总结：运行时间和输入无关
     *      数据移动最少，与数组大小线性关系
     *      对于长度为N的数组，选择排序需要大约 N^2/2 次比较和 N 次交换
     * @param a
     */
    public static void selectionSort(Comparable a[]){
        int N = a.length;
        for(int i = 0; i < N; i++){
            int min = i;
            for(int j =i + 1; j < N; j++){
                if(less(a[j],a[min])) min = j;
            }
            exch(a, i, min);
        }
    }

    /**
     * 插入排序
     * 从第二个元素开始，与之前的元素比较，如果小于之前的一个元素就与之交换，直到无法交换，
     * 然后依次第三、第四直至最后一个元素按相同的条件交换
     * 总结： 对随机排列的长度为N且主键不重复的数组，平均情况下插入排序需要  ~N^2/4 次比较以及 ~N^2/4次交换
     * 最欢情况下需要 ~N^2/2 次比较和 ~N^2/2 次交换，最好情况下需要 N-1 次比较和 0 次交换
     *
     * 提高插入排序速度：将内循环中较大的元素都向右移动而不是总交换两个元素（这样访问数组的次数就减少一半）
     * 适用条件：    对部分有序数组十分高效，也很适合小规模数组
     * @param a
     */
    public static void insertionSort(Comparable[] a){
        int N = a.length;
        for(int i = 1; i < N; i++){
            for(int j = i ; j > 0 ; j--){
                if(less(a[j], a[j-1]))
                    exch(a, j, j-1);
            }
        }
    }

    /**
     * 希尔排序
     * @param a
     */
    public static void shellSort(Comparable[] a){
        int N = a.length;
        int h = 1;
        while(h < N/3) h = 3*h + 1; //1，4，13,40,121，...
        while(h >= 1){  //将数组变为h有序
            for(int i = h; i < N; i++){ //将a[i]插入到a[i-h],a[i-2*h],a[i-3*h]...之中
                for(int j = i; j >= h && less(a[j], a[j-h]); j -=h){
                    exch(a, j, j-h);
                }
            }
            h = h/3;
        }
    }

    /**
     * 原地归并的抽象方法
     * a[] 以mid为界已经部分有序
     * @param a
     */
    public static void merge(Comparable[] a, int lo, int mid, int hi){
        int i = lo, j = mid + 1;
        for(int k = lo; k <= hi; k++){
            aux[k] = a[k];
        }

        for(int k = lo; k <= hi; k++){
            if(i > mid)                   a[k] = aux[j++];
            else if(j > hi)               a[k] = aux[i++];
            else if(less(aux[j], aux[i])) a[k] = aux[j++];
            else                          a[k] = aux[i++];
        }
    }

    /**
     * 自顶向下的归并排序
     * @param a
     */
/*    public static void merge2Sort(Comparable[] a){

    }*/

    //比较v 是否小于t
    private static boolean less(Comparable v, Comparable t){
        return v.compareTo(t) < 0;
    }

    //交换a[i] with a[j]
    private static void exch(Comparable[] a, int i, int j){
        Comparable temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    //在单行中打印数组
    private static void show(Comparable a[]){
        for(Comparable i : a ){
            System.out.print(i + " ");
        }
        System.out.println();
    }

    //测试数组元素是否有序
    private static boolean isSorted(Comparable[] a){
        for(int i = 1; i < a.length; i++){
            if(less(a[i], a[i-1])) return false;
        }
        return true;
    }

    public static void main(String[] args){
        String[] a = "a d f g j e h l n p".split(" ");
        aux = new String[a.length];
        //selectionSort(a);
        //insertionSort(a);
        //merge(a,0,4,9);
        show(a);
    }
}
