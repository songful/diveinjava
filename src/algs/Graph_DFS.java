package algs;

/**
 * 无向图深度优先遍历
 * @author Songful
 *
 */
public class Graph_DFS {
	private boolean[] marked;
	private int count;

	public Graph_DFS(Graph g, int s){
		marked = new boolean[g.V()];
		dfs(g, s);
	}

	private void dfs(Graph g, int v){
		marked[v] = true;
		count++;
		for(int w : g.adj(v)){
			if(!marked[w]) dfs(g, w);
		}
	}

	public boolean marked(int w){
		return marked[w];
	}

	public int count(){
		return count;
	}
}
