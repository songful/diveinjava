package Interviews;
/**
 * @author Songful
 *
 */
public class Encryption {

	public static String encrypt(String a) {
		int n = a.length();
		char[] bChar = new char[n];
		for (int i = 0; i < n; i++) {
			char aChar = a.charAt(i);
			aChar = (char) (aChar * 2 + 10);
			if(aChar > 128){
				aChar = (char) (aChar/3);
			}
			bChar[n-1-i] = aChar;
		}
		return new String(bChar);
	}

	public static void main(String[] args) {

		System.out.println(encrypt("abcde"));

	}

}
