package Interviews;
/**
 * 已知数列如下：F[1] = 1, F[2] = 1, F[3] = 3, F[4] = 5, ..., F[n] =F[n-1] + 2*F[n-2]，那么请问F[18] - F[12]
 * @author Songful
 *
 */
public class EvenPlus {

	public static long plus(int n){
		if(n < 1){
			System.out.println("Error prameter");
			return 0;
		}
		if(n == 1){
			return 1;
		}else if(n == 2){
			return 1;
		}else{
			return plus(n-1) + 2*plus(n-2);
		}
	}

	public static void main(String[] args) {
		System.out.println(plus(18) - plus(12));
	}

}
