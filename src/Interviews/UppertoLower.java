package Interviews;

public class UppertoLower {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(chartoc('a'));
	}

	public static char chartoc(char cha) {
		if ('A' > cha || 'Z' < cha) {
			return cha;
		}
		return (char) ((cha - 'A' + 5) % 26 + 'a');
	}
}
