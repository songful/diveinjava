package Interviews;

public class LinkList {
	public Node head;
	public Node current;
	Node pre;
	Node next;


	class Node{
		public int value;
		public Node next;
		public Node(int value){
			this.value = value;
		}

		public boolean hasNext(){
			return this.next != null;
		}
	}

	public void initList(int[] val){
		if(val.length <1) return;
		head = new Node(val[0]);
		current = head;
		for(int i =1;i<val.length;i++){
			current.next = new Node(val[i]);
			current = current.next;
		}
		current = head;
	}

	public void reverseLink(){
		while(current != null){
			next = current.next;
			current.next = pre;
			pre = current;
			current = next;
		}
		current = pre;
		System.out.println();
	}

	public void traverseLink(){
		while (current != null) {
			System.out.print(current.value + " ");
			current = current.next;
		}
		System.out.println();
		current = head;

	}

	public static void main(String[] args) {
		int values[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		LinkList list = new LinkList();
		list.initList(values);
		list.traverseLink();
		list.reverseLink();

		list.traverseLink();
	}

}
