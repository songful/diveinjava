package Interviews;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class HW_PassLine {

	public static int passline(int[] a ){
		int[] line = {0,10,20,30,40,50,60,70,80,90,100};
		int[] p = new int[11];

		for(int i = 0; i <11; i++){
			int count = 0;
			for(int j =0; j<10;j++){
				if(a[j] >= line[i]){
					count++;
				}
			}
			p[i] = count*10;
		}

		int min = 100;
		int k = 0;
		int tmp;
		for(int m =0;m<11 ;m++){
			tmp = p[m] -60;
			if(tmp >= 0 && tmp <= min){
				min = tmp;
				k = m;
			}
		}
		int result = line[k];
		int n = 0;
		while(n < 10){
			if(a[n] < 60){
				return result;
			}else{
				n++;
			}
		}
		return 60;
	}

	public static void main(String[] args) throws IOException{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String aString = "";
//		int[] a = new int[10];
/*		int i = 0;
		while((aString = reader.readLine()) != null && aString.length() != 0){
			a[i] = Integer.parseInt(aString);
			i++;
		}*/
		int[] a = {41,54,22,33,65,6,67,78,89,90};
		System.out.println(passline(a));

	}

}
