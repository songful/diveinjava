package Interviews;

import java.util.ArrayList;
import java.util.Stack;

/**
 * 表达式求值
 * @author Songful
 *
 */
public class Calculate {
	private Stack<String> postfixStack = new Stack<String>();	//表达式栈
	private Stack<Character> opStack = new Stack<Character>();	//运算符栈
	private int [] operatPriority  = new int[] {0,3,2,1,-1,1,0,2};//运用运算符ASCII码-40做索引的运算符优先级

	public static ArrayList<String> getBackOrder(String expression){
		ArrayList<String> midOp = new ArrayList<String>();
		String num = "";
		for(int i =0; i< expression.length();i++){
			if(Character.isDigit(expression.charAt(i)) || expression.charAt(i) == '.'){
				num += expression.charAt(i);
			}else{
				if(num != ""){
					midOp.add(num);
				}
				midOp.add(expression.charAt(i) + "");
				num = "";
			}
		}
		if(num != "")	midOp.add(num);

		ArrayList<String> backOp = new ArrayList<String>();
		Stack<String> stack = new Stack<String>();
		for(int i = 0; i<midOp.size(); i++){
			if(Character.isDigit(midOp.get(i).charAt(0))){
				backOp.add(midOp.get(i));
			}else {
				switch(midOp.get(i).charAt(0)){
					case '(':
						stack.push(midOp.get(i));
						break;
					case ')':
						while(!stack.peek().equals("(")){
							backOp.add(stack.pop());
						}
						stack.pop();
						break;
					default:
						while(!stack.isEmpty() && compare(stack.peek(), midOp.get(i))){
							backOp.add(stack.pop());
						}
						stack.push(midOp.get(i));
						break;
				}
			}
		}
		while(!stack.isEmpty()){
			backOp.add(stack.pop());
		}
		return backOp;
	}


    /**
     * 计算后缀表达式
     * @param postOrder
     * @return
     */
    public Integer calculate(ArrayList<String> postOrder){
        Stack stack = new Stack();
        for (int i = 0; i < postOrder.size(); i++) {
            if(Character.isDigit(postOrder.get(i).charAt(0))){
                stack.push(Integer.parseInt(postOrder.get(i)));
            }else{
                Integer back = (Integer)stack.pop();
                Integer front = (Integer)stack.pop();
                Integer res = 0;
                switch (postOrder.get(i).charAt(0)) {
                case '+':
                    res = front + back;
                    break;
                case '-':
                    res = front - back;
                    break;
                case '*':
                    res = front * back;
                    break;
                case '/':
                    res = front / back;
                    break;
                }
                stack.push(res);
            }
        }
        return (Integer)stack.pop();
    }

	private boolean isOperator(char ch){
		return ch == '+' || ch == '-' || ch == '*' || ch == '/' || ch == '(' || ch == ')';
	}

	/**
     * 利用ASCII码-40做下标去算术符号优先级
     * @param cur
     * @param peek
     * @return
     */
	private boolean compare(char cur, char peek){
		boolean result = false;
		if(operatPriority[cur - 40] >= operatPriority[peek - 40]){
			result = true;
		}
		return result;
	}

	/**
     * 比较运算符等级
     * @param peek
     * @param cur
     * @return
     */
    public static boolean compare(String peek, String cur){
        if("*".equals(peek) && ("/".equals(cur) || "*".equals(cur) ||"+".equals(cur) ||"-".equals(cur))){
            return true;
        }else if("/".equals(peek) && ("/".equals(cur) || "*".equals(cur) ||"+".equals(cur) ||"-".equals(cur))){
            return true;
        }else if("+".equals(peek) && ("+".equals(cur) || "-".equals(cur))){
            return true;
        }else if("-".equals(peek) && ("+".equals(cur) || "-".equals(cur))){
            return true;
        }
        return false;
    }

	public static void main(String[] args) {
		String s = "12+(23*3-56+7)*(2+90)/2";
		Calculate calculate = new Calculate();
		ArrayList<String> arrayList = Calculate.getBackOrder(s);
		for(String aString : arrayList){
			System.out.println(aString);
		}

	}

}
