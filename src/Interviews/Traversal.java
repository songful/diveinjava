package Interviews;

/**
 * 首先，我们看看前序、中序、后序遍历的特性： 前序遍历： 1.访问根节点 2.前序遍历左子树 3.前序遍历右子树 中序遍历： 1.中序遍历左子树
 * 2.访问根节点 3.中序遍历右子树 后序遍历： 1.后序遍历左子树 2.后序遍历右子树 3.访问根节点
 *
 * @author Songful
 *
 */
public class Traversal {
	/**
	 * 已知前序遍历和中序遍历求后序遍历
	 *
	 * 好了，先说说用前序遍历和中序遍历求后序遍历 假设前序遍历为 adbgcefh, 中序遍历为 dgbaechf
	 * 前序遍历是先访问根节点，然后再访问子树的，而中序遍历则先访问左子树再访问根节点 那么把前序的 a 取出来， 然后查找 a 在中序遍历中的位置就得到
	 * dgb a echf 那么我们就知道 dgb 是左子树 echf 是右子树， 因为数量要吻合 所以前序中相应的 dbg 是左子树 cefh
	 * 是右子树
	 *
	 * @param mid
	 * @param pre
	 * @return
	 */
	public static void requestBackTraversal(String mid, String pre) {

		if (pre.length() == 0) {
			return;
		}
		if (pre.length() == 1) {
			System.out.print(pre + " ");
			return;
		}

		// 根节点是前序遍历第一个元素
		int k = find(mid, pre.charAt(0));

		String preTemp = pre.substring(1, k + 1);
		String midTemp = mid.substring(0, k);
		requestBackTraversal(midTemp, preTemp);

		preTemp = pre.substring(k + 1, pre.length());
		midTemp = mid.substring(k + 1, mid.length());
		requestBackTraversal(midTemp, preTemp);

		System.out.print(pre.substring(0, 1) + " ");

	}

	/**
	 * 已知后序遍历和中序遍历求前序遍历 过程差不多，但由于后序遍历是最后才访问根节点的 所以要从后开始搜索，例如上面的例子，后序遍历为
	 * gbdehfca，中序遍历为 dgbaechf 后序遍历中的最后一个元素是根节点，a，然后查找中序中a的位置 把中序遍历分成 dgb a
	 * echf，而因为节点个数要对应 后序遍历分为 gbd ehfc a，gbd为左子树，ehfc为右子树，这样又可以递归计算了
	 *
	 * @param mid
	 * @param back
	 * @return
	 */
	public static void requestPreTraversal(String mid, String back) {
		int len = back.length();

		if (len == 0) {
			return;
		}
		if (len == 1) {
			System.out.print(back + " ");
			return;
		}

		// 根节点是前序遍历最后一个元素
		int k = find(mid, back.charAt(len - 1));
		System.out.print(back.substring(len - 1, len) + " ");

		String preTemp = back.substring(0, k);
		String midTemp = mid.substring(0, k);
		requestPreTraversal(midTemp, preTemp);

		preTemp = back.substring(k, len-1);
		midTemp = mid.substring(k + 1, mid.length());
		requestPreTraversal(midTemp, preTemp);
	}

	static int find(String str, char c) {
		for (int i = 0; i < str.length(); ++i)
			if (c == str.charAt(i))
				return i;
		return -1;
	}

	public static void main(String[] args) {
		requestBackTraversal("dgbaechf", "adbgcefh");
		System.out.println();
		requestPreTraversal("dgbaechf", "gbdehfca");
//		requestPreTraversal("bac", "bca");
	}

}
