package Interviews;

public class QuickSort {

	public static void quickSort(int[] a){
		if(a.length <2) return;
		int max = 0;
		for(int i =1; i<a.length;i++){
			if(a[max] < a[i]){
				max = i;
			}
		}
		exch(a, a.length-1, max);
		quickSort(a, 0, a.length-2);
	}

	private static void quickSort(int[] a, int first,int last){
		int lower = first+1,upper = last;
		exch(a, first, (first +upper)/2);
		int bundle = a[first];
		while(lower <=upper){
			while(bundle >= a[lower]){
				lower++;
			}
			while(bundle < a[upper]){
				upper--;
			}
			if(lower < upper){
				exch(a, lower, upper);
			}else {
				lower++;
			}
		}
		exch(a, upper, first);
		if(first < upper -1){
			quickSort(a, first, upper-1);
		}
		if(upper+1 <last){
			quickSort(a, upper+1, last);
		}
	}

	private static void exch(int[] a, int lower, int upper){
		int tmp = a[lower];
		a[lower] = a[upper];
		a[upper] = tmp;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] a = {1,3,6,4,8,9,2,7,10,25,14,5};
		quickSort(a);
		for(int i: a){
			System.out.print(i + " ");
		}
	}

}
