package Interviews;

import java.util.*;

public class PrimeFactor {

	public static void main(String[] args) {
		/*
		 * int i = 1000045435; Long times = System.nanoTime();
		 * System.out.println(i + "= " + getPrimeFactors(i)); Long timeE =
		 * System.nanoTime(); System.out.print(" --> " + (timeE-times) + "\n");
		 */
		// System.out.print(getPrimeSet(100));
		/*
		 * primeFactors(i); Long timee = System.nanoTime();
		 * System.out.print(" --> " + (timee-timeE));
		 */
		// System.out.print(gcd(124564, 2456));

/*		for (int i = 1; i < 100000; i++) {
			if (isPerfectNumber(i)) {
				System.out.print(i + " ");
			}
		}*/



	}

	/**
	 * 输入某年某月某日，判断这一天是这一年的第几天？
	 * @param year
	 * @param mouth
	 * @param day
	 * @return
	 */
	public static int days(int year, int mouth, int day){
		int a[] = {31,28,31,40,31,30,31,31,30,31,30,31};
		if(isLeapYear(year)) a[1] = 29;

		if(year <= 0) return -1;
		if(mouth<1 || mouth>12) return -1;
		if(day <1 || day > a[mouth -1]) return -1;

		int sumdays = 0;
		for(int i =0; i<=mouth-1; i++){
			sumdays += a[i];
		}
		return sumdays + day;
	}

	/**
	 * 闰年
	 * @param n
	 * @return
	 */
	public static boolean isLeapYear(int n) {
		return (n%4==0 && n%100 != 0) || n%400 ==0;
	}

	/**
	 * 完美数：各个小于它的约数（真约数,列出某数的约数，去掉该数本身，剩下的就是它的真约数）的和等于它本身的自然数叫做完全数（Perfect
	 * number），又称完美数或完备数。
	 * 1+2+3=6；1+2+4+7+14=28
	 *
	 * @param n
	 * @return
	 */
	public static boolean isPerfectNumber(int n) {
		int sum = 0;

		for (int i = 1; i <= n / 2; i++) {
			if (n % i == 0) {
				sum += i;
			}
		}

		return sum == n;
	}

	/**
	 * 求最大公约数和最小公倍数
	 *
	 * @param a
	 * @param b
	 */
	public static void _gcd(int a, int b) {
		int na = a, nb = b; // 令na和nb分别保存a和b的初始值
		int t; // 定义中间变量t
		if (a < b) {
			t = a;
			a = b;
			b = t;
		}
		t = a % b;
		while (t != 0) {
			a = b;
			b = t;
			t = a % b;
		}
		System.out.println("最大公约数是：" + b);
		System.out.println("最小公倍数是：" + na * nb / b);

	}

	public static int gcd(int m, int n) {

		while (true) {
			if ((m = m % n) == 0)
				return n;
			if ((n = n % m) == 0)
				return m;
		}
	}

	/**
	 * 求s=a+aa+aaa+aaaa+aa...a的值，其中a是一个数字。例如2+22+222+2222+22222(此时共有5个数相加)
	 *
	 * @param a
	 * @param n
	 */
	public static void xiangJia(long a, int n) {
		String str = "";
		long s = 0, sum = 0;
		int i;
		for (i = 0; i < n; i++) {
			s = s + (long) Math.pow(10, i) * a;
			sum = sum + s;
			str += s + "+";
		}
		System.out.println("则有：" + str.substring(0, str.length() - 1) + "="
				+ sum);
	}

	/**
	 * 分解质因数
	 *
	 * @param n
	 */
	public static void primeFactors(int n) {
		System.out.print(n + "=");
		for (int k = 2; k < n;) {
			if (n % k == 0) {
				System.out.print(k + "*");
				n /= k;
			} else {
				k++;
			}
		}
		System.out.println(n);
	}

	public static List<Integer> getPrimeFactors(int x) {
		List<Integer> primeFatorList = new ArrayList<Integer>();
		Set<Integer> primeSet = getPrimeSet(x);
		Iterator<Integer> primeIterator = primeSet.iterator();
		int f = 2;
		if (primeIterator.hasNext()) {
			f = primeIterator.next();
		}

		while (true) {
			if (primeSet.contains(x)) {
				primeFatorList.add(x);
				break;
			}

			while (x % f != 0) {
				f = primeIterator.next();
			}

			primeFatorList.add(f);
			x /= f;
		}

		return primeFatorList;
	}

	/**
	 * 求给定数字之内的质数集合
	 *
	 * @param n
	 * @return
	 */
	public static Set<Integer> getPrimeSet(int n) {
		/**
		 * BitSet类 大小可动态改变, 取值为true或false的位集合。用于表示一组布尔标志. 此类实现了一个按需增长的位向量。位 set
		 * 的每个组件都有一个 boolean 值。用非负的整数将 BitSet的位编入索引。可以对每个编入索引的位进行测试、设置或者清除。
		 * 通过逻辑与、逻辑或和逻辑异或操作，可以使用一个 BitSet 修改另一个BitSet 的内容。 默认情况下，set 中所有位的初始值都是
		 * false。
		 */
		BitSet primeBitSet = new BitSet(n);
		for (int i = 1; i <= n; i++) {
			primeBitSet.set(i); // 将指定索引处的位设置为 true
		}

		int s = (int) Math.sqrt(n);
		// 从2开始，将i 的倍数对应的位向量设为false 然后3的倍数，5的倍数。。。
		for (int i = 2; i <= s; i++) {
			if (primeBitSet.get(i)) {
				for (int j = i * 2; j <= n; j += i) {
					primeBitSet.set(j, false);
				}
			}
		}

		Set<Integer> primeSet = new LinkedHashSet<Integer>();
		for (int i = 2; i <= n; i++) {
			if (primeBitSet.get(i)) {
				primeSet.add(i);
			}
		}

		return primeSet;
	}

	/*
	 * public static void fengjie(int n) { for (int i = 2; i <= n / 2; i++) { if
	 * (n % i == 0) { System.out.print(i + "*"); fengjie(n / i); }
	 *
	 * } System.out.print(n); return; //System.exit(0);// /不能少这句，否则结果会出错
	 *
	 * }
	 */
}
