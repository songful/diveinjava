package Interviews;

/**
 * 题目描述： 求500万以内的所有亲和数 如果两个数a和b，a的所有真因数之和等于b,b的所有真因数之和等于a,则称a,b是一对亲和数。
 * 例如220和284，1184和1210，2620和2924。
 *
 * @author Songful
 *
 */
public class Amicable {

	public static void main(String[] args) {
		amicable(5000000);

	}

	public static void amicable(int N){
		int[] sum = new int[N+1];
		int i, j ;
		for(int m = 0; m<sum.length; m++){
			sum[m] = 1;
		}
		for(i = 2; i+i<=N;i++){
			j = i + i;
			while(j <= N){
				sum[j] += i;
				j += i;
			}
		}

		for(i = 2; i <= N; i++){
			if(sum[i] > i && sum[i] <=N && sum[sum[i]] == i){
				System.out.println(i +" "+ sum[i]);
			}
		}
	}
}
