package Interviews;

public class HuaweiQue {

	/**
	 * 验证13位电话号码
	 *
	 * @param phone
	 * @return 0:ok, 1:长度非法, 2:非86开头 ,3:含非法字符
	 */
	public static int phoneNum(String phone) {
		char[] num = phone.toCharArray();
		if (num.length == 13) {
			if (num[0] == '8' && num[1] == '6') {
				int i = 2;
				while (i < 13) {
					if (num[i] >= '0' && num[i] <= '9') {
						i++;
					} else {
						return 3;
					}
				}
				return 0;
			} else {
				return 2;
			}
		} else {
			return 1;
		}

	}

	public static void stringFilter(char[] inputStr, long length,
			char[] outputStr) {
		int[] a = new int[26];
		int len = 0;
		for (int i = 0; i < length; i++) {
			int temp = 0;
			temp = inputStr[i] - 'a';
			if (a[temp] == 0) {
				outputStr[len++] = inputStr[i];
				a[temp] = 1;
			} else {
				continue;
			}
		}
	}

	private static void stringZip(char[] inputStr, long length, char[] outputStr) {
		int i = 0;
		int len = 0;
		while (i < length) {
			int time = 1;
			char c = inputStr[i++];
			while (i < length && c == inputStr[i]) {
				time++;
				i++;
			}
			if (time == 1) {
				outputStr[len++] = c;
			} else if (time <= 9) {
				outputStr[len++] = (char) (time + '0');
				outputStr[len++] = c;
			} else if (time <= 99) {
				outputStr[len++] = (char) (time / 10 + '0');
				outputStr[len++] = (char) (time % 10 + '0');
				outputStr[len++] = c;
			} else {
				// 当连续重复的字符数超过99时，用一个“?”来表示。
				outputStr[len++] = (char) 63;
				outputStr[len++] = c;
			}
		}
	}

	public static void arithmetic(char[] inputStr, long length, char[] outputStr){

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
/*		char[] inputStr = "aabassfffffffffffffffssddddfff".toCharArray();
		long length = inputStr.length;
		char[] outputStr = new char[(int) length];
		stringZip(inputStr, length, outputStr);
		for(int i=0;i<outputStr.length;i++){
            System.out.print(outputStr[i]);
        }*/
		char[] a = {'1','2',' ',' ','+','+',' ',' ','3'};
		String aString = String.valueOf(a);
		String[] aStrings = aString.split("");
		for(String string :aStrings){
			System.out.print(string);
		}
	}

}
