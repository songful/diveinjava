package Interviews;

import java.util.Arrays;
import java.util.List;

/**
 * 假设x是这样的自然数，x包含数字是5和7（如57），那么在3000以内（小于3000 ），求所有这样的X累加之和
 * @author Songful
 *
 */
public class CVTE_3 {
	public static void main(String[] args) {
		int sum = 0;
		for(int i =1; i< 3000; i++){
			if(match(i)){
				System.out.print(i + " ");
				sum += i;
			}
		}
		System.out.println(sum);
	}

	public static boolean match(int num){
		String[] str = String.valueOf(num).split("");
		List strList = Arrays.asList(str);
		if(strList.contains("5") && strList.contains("7")  ){
			return true;
		}
		return false;
	}
}
